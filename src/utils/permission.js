// 自定义指令的注册
import Vue from 'vue'
import router from '@/router'
Vue.directive('permission', {
  inserted (el, binding) {
    // console.log(el)
    // console.log(binding)
    const action = binding.value.action
    const effect = binding.value.effect
    // 判断 当前的路由所对应的组件中 如何判断用户是否具备action的权限
    // console.log(router.currentRoute.meta, '按钮权限')
    if (router.currentRoute.meta.indexOf(action) === -1) { // 等于-1说明没找到 不具备权限
      if (effect === 'disabled') { // 禁用操作
        el.disabled = true // 开启disabled
        el.classList.add('is-disabled') // 开启disabled，element ui 需要添加is-disabled
      } else { // 删除操作
        el.parentNode.removeChild(el) // el 当前元素  的父节点 ，移除子元素 el
      }
    }
  }
})
